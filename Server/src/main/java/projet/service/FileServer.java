package projet.service;

import projet.config.Config;

import java.net.ServerSocket;
import java.net.Socket;
public class FileServer {

    private static ServerSocket serverSocket;
    private static Socket clientSocket = null;

    public FileServer() {
        Config config = new Config();
        try {
            serverSocket = new ServerSocket(config.getPort());
            System.out.println("Heyy!! Server started on port  : " + serverSocket.getLocalPort() +
                    "        And with this InetAdress : " + serverSocket.getInetAddress() +
                    "        And with this localSocketAdress : " + serverSocket.getLocalSocketAddress());
        } catch (Exception e) {
            System.err.println("Port already in use.");
            System.exit(1);
        }


        while (true) {
            try {
                clientSocket = serverSocket.accept();
                System.out.println("Accepted connection : " + clientSocket);

                Thread t = new Thread(new ServiceClient(clientSocket));

                t.start();

            } catch (Exception e) {
                System.err.println("Error in connection attempt.");
            }
        }
    }
}
