//pom.xml hérite du parent spring-boot-starter-parent qui nous permet de ne plus nous
// soucier des versions des dépendances et de leur compatibilité
//CE SPRINGBOOT Tomcat : intégré, va nous permettre de lancer notre application en
// exécutant tout simplement le jar sans avoir à le déployer dans un serveur d'application.
package projet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import projet.service.FileServer;

@SpringBootApplication
public class MyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyApplication.class, args);
        FileServer fileServer = new FileServer();
    }

}
