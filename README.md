# [FILESENDER - JAVA]
* **1.Introduction**
* **2.Installation**
* **3.Server FILE**
* **4.Docker**

# Introduction

Repository recensant l'ensemble des fichiers nécessaire au bon fonctionnement du FileSender en JAVA. L'objectif de cette partie est de transferet via le protocol WebSocket, un ou plusieurs fichiers entre un client File et un server File. Une fois le transfert effectuer, le client supprime le ou les fichiers qui ont été envoyés au server (Cela revient a envoyer une copie au serveur, il faut donc supprimer le fichier original). 

Prérequis ---- Dernière version de JAVA à jour
        Ici pour l'installer :
        
Linux --> " sudo apt-get install openjdk-7-jdk "

Windows --> https://www.java.com/fr/download/

# Installation et utilisation

Créer un dossier dans lequel nous allons mettre tout nos fichiers, client, server ...

## Activer le serveur

Déplacez vous dans le dossier `Server`.
--> Pour trouver les sources, vous pouvez vous déplacer ici: 

    ```ruby
    cd src\main\java\projet\
     ```
    Dans le package `config`, vous pouvez retrouvez la classe `Config.java` où toutes les configurations du serveur se trouvent. Nottament le 'port' du server sur lequel il sera lancé. Vous pouvez les modifier à votre guise selon vos besoins.
    Sinon, vous pouvez retrouvez le `.JAR` exécutable dans le dossier suivant:
    
    ```ruby
    cd target\
    ```
    Vous pouvez exécuter et lancer le serveur avec cette commande-ci :
    ```ruby
    java -jar JAVAServerSendFile.jar
    ```


## Activer le client

Déplacez vous dans le dossier `Client`.
--> Pour trouver les sources, vous pouvez vous déplacer ici: 

    ```
    cd src\main\java\projet\
    ```
    Dans le package `config`, vous pouvez retrouvez la classe `Config.java` où toutes les configurations du serveur se trouvent. Nottament `l'ip` du server File, ainsi que le `port`avec lequel communiquer. Vous pouvez les modifier à votre guise selon vos besoins.
    Sinon, vous pouvez retrouvez le `.JAR` exécutable dans le dossier suivant:
    ```
    cd client\target\
    ```
    Vous pouvez exécuter et lancer le serveur avec cette commande-ci :
    ```
    java -jar JAVAClientFileSend.jar fichierATransfer1.txt fichierATransferer2.txt
    ```
    Ici le ou les fichier que vous voulez transmettre se récupère grâce aux arguments passés en ligne de commande. Ces fichiers seront ensuite transmis au Server File. Une fois transmis, les serveur se supprimeront de l'emplacement d'origine du client. Vous pouvez passer x fichiers dans la ligne de commande, il suffit juste d'espacer le nom des fichiers par des espaces. L'extension des fichiers est obligatoire pour le bon fonctionnement de l'application Client.


## Docker

Builder l'image du server java recevant les fichiers audios, à l'aide du Dockerfile et de cette commande en se placant à la racine du projet :
`docker build -t server-java-file .`

Ensuite lancer le conteneur de l'application :
`docker run -d --restart=always -p 8888:8888 -v "$PWD":/usr/serverfile --name server-java-file java-server-file`
