FROM openjdk:15
COPY . /usr/serverfile
WORKDIR /usr/serverfile
EXPOSE 8888
CMD ["java","-jar","Server/target/JAVAServerSendFile-v1.jar"]
